//
//  ViewController.swift
//  AR Gojek (Driver)
//
//  Created by octagon studio on 07/10/18.
//  Copyright © 2018 Cordova. All rights reserved.
//

import UIKit
import CoreLocation
import PusherSwift
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    let acceptOrderButton = UIButton(type: UIButton.ButtonType.system) as UIButton
    let xPosition: CGFloat = 270
    let yPosition: CGFloat = 546
    let buttonWidth: CGFloat = 110
    let buttonHeight: CGFloat = 121
    
    var locationManager = CLLocationManager()
    var userHeading = 0.0
    
    let pusher = Pusher(
        key: "70cdf5e5d41623d28576",
        options: PusherClientOptions(
            authMethod: .inline(secret: "86e337cefb39fa82399d"),
            host: .cluster("ap1")
        )
    )
    
    var channel: PusherChannel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        darkNIB()
        drawOrderButton()

    }
    
    func drawOrderButton(){
        acceptOrderButton.frame = CGRect(x: xPosition, y: yPosition, width: buttonWidth, height: buttonHeight)
        acceptOrderButton.backgroundColor = UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
        acceptOrderButton.setTitle("Accept", for: .normal)
        acceptOrderButton.setTitleColor(UIColor.white, for: .normal)
        acceptOrderButton.layer.cornerRadius = 0
        acceptOrderButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.light)
        acceptOrderButton.addTarget(self, action: #selector(ViewController.buttonTapped), for: .touchUpInside)
        self.view.addSubview(acceptOrderButton)
    
    }
    
    @objc func buttonTapped(_ sender: UIButton!){
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
            locationManager.distanceFilter = 2.0
        }
        
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
        // subscribe to channel and connect
        channel = pusher.subscribe("private-channel")
        pusher.connect()
    }
    
    //MARK: - CUSTOM UI ELEMENT DESIGN
    //1. Dark Status Bar
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //2. Dark Navigation Item Bar and Text
    func darkNIB(){
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

    }

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if (newHeading.headingAccuracy < 0) {
            return
        }
        
        // Use the true heading if it is valid
        self.userHeading = ((newHeading.trueHeading > 0) ?
            newHeading.trueHeading : newHeading.magneticHeading)
 
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else {
            return
        }
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        mapView.mapType = MKMapType.standard
        
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = locValue
        annotation.title = "Cordova"
        annotation.subtitle = "current location"
        mapView.addAnnotation(annotation)
        
        sendPusherEvent(location);
        }
    
    func sendPusherEvent(_ location: CLLocation) {
        channel?.trigger(eventName: "client-new-location",
                        data: [
                            "latitude": String(location.coordinate.latitude),
                            "longitude": String(location.coordinate.longitude),
                            "heading": String(self.userHeading)
            ]
        )
    }
    

}

